import { Injectable } from '@angular/core';
import { Http , Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { AngularFireDatabase } from 'angularfire2/database';


@Injectable()
export class ProductsService {
  http:Http;

  getProductsFire(){
 
    return this.db.list('/products').valueChanges();
  }

  getProducts(){
    return this.http.get(environment.url + 'products');
  }

  putProduct(data,key){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    
    let params = new HttpParams().append('name',data.name).append('price',data.price);
    return this.http.put(environment.url + 'products/'+ key,params.toString(), options);
  }

  getProduct(id){
     return this.http.get(environment.url+'products/'+ id);
  }

  constructor(http:Http,  private db:AngularFireDatabase) {
    this.http = http;
   }

}
