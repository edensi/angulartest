import { Component, OnInit } from '@angular/core';
import { ProductsService } from './../products/products.service';

@Component({
  selector: 'fproducts',
  templateUrl: './fproducts.component.html',
  styleUrls: ['./fproducts.component.css']
})
export class FproductsComponent implements OnInit {

  products;

  constructor(private service:ProductsService) { }

  ngOnInit() {
    this.service.getProductsFire().subscribe(response=>{
        console.log(response);
        this.products = response;
    });
  }
}
