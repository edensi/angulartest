import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { environment } from './../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ProductsService } from './products/products.service';
import { EditProductComponent } from './products/edit-product/edit-product.component';
import { FproductsComponent } from './fproducts/fproducts.component';
import { SearchResultsComponent } from "./products/search-results/search-results.component";


@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    LoginComponent,
    NotFoundComponent,
    NavigationComponent,
    EditProductComponent,
    FproductsComponent,
    SearchResultsComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
            {path: '', component: ProductsComponent},
            {path: 'login', component: LoginComponent},
            {path: 'edit-product/:id', component: EditProductComponent},
            {path: 'fproducts', component: FproductsComponent},
            {path: '**', component: NotFoundComponent}
          ])
  ],
  providers: [
    ProductsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
