// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
     url:'http://localhost/angular/exam/slim/',
    firebase:{
    apiKey: "AIzaSyBiXUsBIjQX1Xfk4f4Pcf5_sUe7sbBShig",
    authDomain: "exam-8936b.firebaseapp.com",
    databaseURL: "https://exam-8936b.firebaseio.com",
    projectId: "exam-8936b",
    storageBucket: "exam-8936b.appspot.com",
    messagingSenderId: "823015319635"
  }
};
